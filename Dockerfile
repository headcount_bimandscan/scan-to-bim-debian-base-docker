#
# TEMP(neil)
#

FROM debian:buster-slim
MAINTAINER Neil Hyland <neil.hyland@bimandscan.com>

# Prepare environment:
COPY ./sources.list /etc/apt/sources.list
RUN mkdir -p /newlibs/autogen
COPY ./gco /newlibs/autogen/gco

# Build library & tools:
COPY ./DockerBuildDev.sh /newlibs/autogen/
RUN chmod +x "/newlibs/autogen/DockerBuildDev.sh"
RUN bash -c "/newlibs/autogen/DockerBuildDev.sh"

# Install OptiX:
COPY ./optix.zip /newlibs/autogen/
RUN unzip /newlibs/autogen/optix.zip -d /

# Build rest:
COPY ./DockerBuildDev2.sh /newlibs/autogen/
RUN chmod +x "/newlibs/autogen/DockerBuildDev2.sh"
RUN bash -c "/newlibs/autogen/DockerBuildDev2.sh"
