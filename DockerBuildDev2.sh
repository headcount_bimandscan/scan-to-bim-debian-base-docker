#!/bin/sh

#
# 2018 © BIM & Scan Ltd.
# See 'LICENCE.md' in the project root for more information.
#

set -e
EMBREE_VERSION="3.2.0"
IFCOPENSHELL_VERSION="0.5.0rc0"
#"0.5.0-preview2"

# Build IFCOpenShell:
wget "https://github.com/IfcOpenShell/IfcOpenShell/archive/v$IFCOPENSHELL_VERSION.tar.gz" -P "/newlibs"
tar xf "/newlibs/v$IFCOPENSHELL_VERSION.tar.gz" --directory "/newlibs"

mkdir -p "/newlibs/IfcOpenShell-$IFCOPENSHELL_VERSION/build"
cd "/newlibs/IfcOpenShell-$IFCOPENSHELL_VERSION/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++17" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -std=gnu11 " \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DOCC_LIBRARY_DIR="/usr/lib/x86_64-linux-gnu" \
                          -DCOLLADA_SUPPORT=OFF \
                          -DBUILD_IFCPYTHON=OFF \
                          -DUNICODE_SUPPORT=OFF \
                          -DENABLE_BUILD_OPTIMIZATIONS=ON \
                          -DBUILD_IFCMAX=OFF \
                          -DBUILD_EXAMPLES=OFF \
                          -DBUILD_SHARED_LIBS=ON \
                          -DOCC_INCLUDE_DIR="/usr/include/opencascade" \
                          "../cmake"

make -j2
make install/strip

# Build Embree:
wget "https://github.com/embree/embree/archive/v$EMBREE_VERSION.tar.gz" -P "/newlibs"
tar xf "/newlibs/v$EMBREE_VERSION.tar.gz" --directory "/newlibs"

mkdir -p "/newlibs/embree-$EMBREE_VERSION/build"
cd "/newlibs/embree-$EMBREE_VERSION/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++17" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -std=gnu11 " \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DBUILD_TESTING=OFF \
                          -DEMBREE_RAY_MASK=OFF \
                          -DEMBREE_TUTORIALS=OFF \
                          -DEMBREE_TUTORIALS_IMAGE_MAGICK=OFF \
                          -DEMBREE_TUTORIALS_LIBJPEG=OFF \
                          -DEMBREE_TUTORIALS_LIBPNG=OFF \
                          -DEMBREE_ISPC_SUPPORT=OFF \
                          ".."

make -j2
make install/strip

# Install static & header-only libraries:
mkdir -p "/newlibs/autogen/gco/build"
cd "/newlibs/autogen/gco/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++14" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -std=gnu11" \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          ".."

make -j2
make install/strip
mv /usr/local/include/gco /usr/local/include/GCO

git clone "https://bitbucket.org/headcount_bimandscan/libe57.git" "/newlibs/autogen/libe57"
mkdir -p "/newlibs/autogen/libe57/build"
cd "/newlibs/autogen/libe57/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++14" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -std=gnu11" \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          ".."

make -j2
make install/strip

git clone "https://bitbucket.org/headcount_bimandscan/e57-pcl-import.git" "/newlibs/autogen/e57-pcl"
mkdir -p "/newlibs/autogen/e57-pcl/build"
cd "/newlibs/autogen/e57-pcl/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++14" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -std=gnu11" \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          ".."

make -j2
make install/strip

git clone "https://bitbucket.org/headcount_bimandscan/primitiveshapes.git" "/newslibs/autogen/primitiveshapes"
mkdir -p "/newslibs/autogen/primitiveshapes/build"
cd "/newslibs/autogen/primitiveshapes/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++03 -march=native -fpermissive" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -march=native -fpermissive" \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          ".."

make -j2
make install/strip

git clone "https://www.graphics.rwth-aachen.de:9000/OpenMesh/OpenMesh.git" "/newslibs/autogen/openmesh"
mkdir -p "/newslibs/autogen/openmesh/build"
cd "/newslibs/autogen/openmesh/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++14 -march=native" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -march=native -std=gnu11" \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DOPENMESH_BUILD_PYTHON_BINDINGS="OFF" \
                          -DBUILD_APPS="OFF" \
                          ".."

make -j2
make install/strip

git clone "https://bitbucket.org/headcount_bimandscan/cartan.git" "/newslibs/autogen/cartan"
mkdir -p "/newslibs/autogen/cartan/build"
cd "/newslibs/autogen/cartan/build"

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                          -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++14 -march=native" \
                          -DCMAKE_C_FLAGS="-m64 -fPIC -march=native -std=gnu11" \
                          -DCMAKE_CXX_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          -DCMAKE_C_FLAGS_RELEASE="-g0 -O3 -DNDEBUG" \
                          ".."

make -j2
make install/strip
