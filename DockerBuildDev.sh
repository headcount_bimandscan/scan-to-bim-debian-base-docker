#!/bin/sh

#
# 2018 © BIM & Scan Ltd.
# See 'LICENCE.md' in the project root for more information.
#

apt update -y
apt install -y libssl-dev \
               libcurl4-openssl-dev \
               libboost-filesystem-dev \
               libboost-program-options-dev \
               libboost-regex-dev \
               libboost-date-time-dev \
               libboost-iostreams-dev \
               libcgal-dev \
               liblapack-dev \
               libocct-foundation-dev \
               libocct-modeling-data-dev \
               libocct-modeling-algorithms-dev \
               libocct-data-exchange-dev \
               libtbb-dev \
               libicu-dev \
               libxerces-c-dev \
               libgomp1 \
               build-essential \
               libarchive-dev \
               cmake \
               git \
               wget \
               autoconf \
               automake \
               doxygen \
               tar \
               libstxxl-dev \
               libspdlog-dev \
               libgtest-dev \
               libblas-dev \
               liblapack-dev \
               libpcl-dev \
               liblemon-dev \
               nvidia-cuda-dev \
               unzip \
               zip \
               libcereal-dev \
               libc6-dev
